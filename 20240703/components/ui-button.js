class  UiButton extends HTMLElement {
    // static style = `
    //     .btn {
    //         display: inline-block;
    //         border: 1px solid #ddd;
    //         padding: 10px 15px;
    //         cursor: pointer;
    //     }
    //     .btn:hover {
    //         color: #fff;
    //         background: #333;
    //     }
    // `;
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        // this.styling();
        this.render();
    }
    // styling() {
    //     this.stylesheet = document.createElement('style');
    //     this.stylesheet.textContent = this.constructor.style;
    //     this.shadowRoot.appendChild(this.stylesheet);
    // }
    render() {
        this.btn = document.createElement('div');
        this.btn.textContent = this.getAttribute('text');
        this.btn.classList += `btn ${this.getAttribute('color')}`;
        // 對 shadow DOM 做事情
        this.shadowRoot.appendChild(this.btn);
    }
    connectedCallback() {
        // didMount
        console.log("ui-button added to page");
    }
}

customElements.define('ui-button', UiButton);
export default { UiButton };