class UiHero extends HTMLElement {
    static style = `
        img.pic {
            display: block;
            width: 100px;
            height: auto;
            background: cover;
            margin-top: 20px;
        }
    `;
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.styling();
        this.render();
    }
    styling() {
        this.stylesheet = document.createElement('style');
        this.stylesheet.textContent = this.constructor.style;
        this.shadowRoot.appendChild(this.stylesheet);
    }
    render() {
        this.hero = document.createElement('img');
        this.hero.setAttribute('src', this.getAttribute('src'));
        this.hero.classList += `pic`;
        // 對 shadow DOM 做事情
        this.shadowRoot.appendChild(this.hero);
    }
    connectedCallback() {
        // didMount
        console.log("hero added to page");
    }
}

customElements.define('ui-hero', UiHero);

export default { UiHero };