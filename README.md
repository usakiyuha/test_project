# web_components（筆記）

> 必須本機 run 才能正常顯示（例如：Go Live），否則會出現這種不同源的警告，原因是你根本無 host 來源，所以他顯示 null

![](error_CORS.png)

### 2022/3/25

- 資料來源：

1. Web Components 原生組件簡介與實作 at 2021/05/29：https://www.youtube.com/watch?v=Ne1sZZFNfoc
2. lifecycle callbacks: https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements
3. 阮一峰的 web component 解釋：https://www.bookstack.cn/read/webapi-tutorial/spilt.2.docs-webcomponents.md
4. MDN github: https://github.com/mdn/web-components-examples
5. globalThis & window & this 不同: https://ithelp.ithome.com.tw/articles/10249876

- HTML 檔案：

1. 如何建構出一個 web component
2. extends HTMLElement
3. super()的意思，位置跟 this 的使用
4. this.attachShadow({mode: 'open'});
5. window.customElements.define('my-button', MyButton);
6. this.styling(); this.render();

### 2024/7/3

[HackMD](https://hackmd.io/088PTXtGTCG6Vc8wlJqmFA)
